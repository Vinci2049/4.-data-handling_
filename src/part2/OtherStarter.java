package part2;

import part2.otherDemo.OtherDemoServiceImpl;

public class OtherStarter {

    public static void main(String[] args) {

        OtherDemoServiceImpl otherDemoService = new OtherDemoServiceImpl();
        otherDemoService.printAreaOfCircle();
        otherDemoService.printNumberIsSum();
        otherDemoService.printMaxMin();

    }
}
