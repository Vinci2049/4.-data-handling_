package part2.otherDemo;

public interface OtherDemoService {

    void printAreaOfCircle();

    void printNumberIsSum();

    void printMaxMin();

}
