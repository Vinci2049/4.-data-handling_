package part2.otherDemo;

import part2.otherService.OtherService;

import java.math.BigDecimal;
import java.util.Random;

public class OtherDemoServiceImpl implements OtherDemoService {

    @Override
    public void printAreaOfCircle() {
        Random random = new Random(100);
        double radius = random.nextDouble();
        BigDecimal area = OtherService.getAreaOfCircle(radius);
        System.out.println("2.1 Если радиус круга = " + radius + ", то его площадь = " + area);
        System.out.println();
    }

    @Override
    public void printNumberIsSum() {

        System.out.println("2.2 Проверяем сумму чисел:");

        if (OtherService.NumberIsSum("0,1", "0,15", "0,25")) {
            System.out.println("Третье число является суммой двух первых");
        } else {
            System.out.println("Третье число не является суммой двух первых");
        }
        System.out.println();
    }

    @Override
    public void printMaxMin() {

        System.out.println("2.3 Находим минимум и максимум из трех чисел: ");
        Random random = new Random();
        int number1 = random.nextInt(100);
        int number2 = random.nextInt(100);
        int number3 = random.nextInt(100);

        System.out.println("Число 1: " + number1);
        System.out.println("Число 2: " + number2);
        System.out.println("Число 3: " + number3);

        int min = OtherService.getMin(number1, number2, number3);
        int max = OtherService.getMax(number1, number2, number3);

        System.out.println("Минимум: " + min);
        System.out.println("Максимум: " + max);

        System.out.println();

    }
}
