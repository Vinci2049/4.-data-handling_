package part2.otherService;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Double.parseDouble;

public class OtherService {

    /*
    2.1.  Необходимо посчитать площадь круга с указанным радиусом с точностью 50 знаков после запятой
     */

    public static BigDecimal getAreaOfCircle(double radius) {

        MathContext mathContext = new MathContext(50);

        final BigDecimal PI = new BigDecimal(3.14159265358979323846264338327950288419716939937510, mathContext);
        BigDecimal areaOfCircle = new BigDecimal(radius * radius, mathContext);
        areaOfCircle = areaOfCircle.multiply(PI, mathContext);

        return areaOfCircle;
    }


    /*
    Даны три числа, например, 0.1, 0.15 и 0.25. Числа даны в виде строки.
    Необходимо ответить, является ли третье число суммой двух первых.
    * Учесть локаль пользователя и разделитель целой-дробной частей в данных строках
    */
    public static boolean NumberIsSum(String stringNumber1, String stringNumber2, String stringNumber3) {

        if (isValidNumber(stringNumber1) && isValidNumber(stringNumber2) && isValidNumber(stringNumber3)) {

            NumberFormat format = NumberFormat.getInstance(Locale.getDefault());

            try {
                Number number1 = format.parse(stringNumber1);
                double doubleNumber1 = number1.doubleValue();

                Number number2 = format.parse(stringNumber2);
                double doubleNumber2 = number2.doubleValue();

                Number number3 = format.parse(stringNumber3);
                double doubleNumber3 = number3.doubleValue();

                if ((doubleNumber1 + doubleNumber2) - doubleNumber3 < 0.00005) {
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Числа для проверки суммирования заданы некорректно");
        }

        return false;
    }

    private static boolean isValidNumber(String string) {

        DecimalFormat format = new DecimalFormat();
        NumberFormat numberFormat = DecimalFormat.getNumberInstance();
        DecimalFormatSymbols symbols = format.getDecimalFormatSymbols();

        char sep = symbols.getDecimalSeparator();

        String regex;

        if (sep == '.') {
            regex = "\\-?\\d+(\\.\\d{0,})?";
        } else {
            regex = "\\-?\\d+(\\,\\d{0,})?";
        }

        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(string);

        System.out.println("Число 1: " + string + " это корректное число: " + m.matches());

        return m.matches();

    }

    /*
    2.3. Даны три числа. Нужно найти минимум и максимум не используя условный и тернарный операторы
    */

    public static int getMin(int number1, int number2, int number3) {

        return Math.min(Math.min(number1, number2), Math.min(number2, number3));

    }

    public static int getMax(int number1, int number2, int number3) {

        return Math.max(Math.max(number1, number2), Math.max(number2, number3));

    }

}
