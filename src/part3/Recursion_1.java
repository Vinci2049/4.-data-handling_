package part3;

public class Recursion_1 {

    /* Given a non-negative int n, return the count of the occurrences of 7 as a digit,
    so for example 717 yields 2. (no loops).
    Note that mod (%) by 10 yields the rightmost digit (126 % 10 is 6),
    while divide (/) by 10 removes the rightmost digit (126 / 10 is 12).

    count7(717) → 2
    count7(7) → 1
    count7(123) → 0 */

    public int count7(int n) {

        if (n < 10) return n == 7 ? 1 : 0;

        return (n % 10 == 7 ? 1 : 0) + count7(n / 10);

    }


    /* Given a string that contains a single pair of parenthesis,
    compute recursively a new string made of only of the parenthesis and their contents,
    so "xyz(abc)123" yields "(abc)".

    parenBit("xyz(abc)123") → "(abc)"
    parenBit("x(hello)") → "(hello)"
    parenBit("(xy)1") → "(xy)" */

    public String parenBit(String str) {

        if (str.charAt(0) == '(' && str.charAt(str.length() - 1) == ')') {
            return str;
        }

        return str.charAt(0) == '(' ?
                parenBit(str.substring(0, str.length() - 1)) :
                parenBit(str.substring(1, str.length()));

    }


    /* Given a string, compute recursively a new string where all the adjacent chars are now separated by a "*".

    allStar("hello") → "h*e*l*l*o"
    allStar("abc") → "a*b*c"
    allStar("ab") → "a*b" */

    public String allStar(String str) {

        int i = 1;
        while (i < str.length()) {
            if (str.charAt(i) != '*') {
                return allStar(str.substring(0, i) + '*' + str.substring(i));
            }
            i += 2;
        }
        return str;
    }


}