package part1.dateService;

import java.util.Date;

import static java.lang.Math.abs;

public class DateServiceImpl implements DateService {

    @Override
    public void printAge(Date dateOfBirth) {

        Date currentDate = new Date();

        System.out.println("Дата рождения: " + dateOfBirth);
        System.out.println();

        long ageInMilliseconds = currentDate.getTime() - dateOfBirth.getTime();

        long ageInSeconds = ageInMilliseconds / 1000;
        System.out.println("Возраст в секундах: " + ageInSeconds);

        long ageInMinutes = ageInMilliseconds / (1000 * 60);
        System.out.println("Возраст в минутах: " + ageInMinutes);

        long ageInHours = ageInMilliseconds / (1000 * 60 * 60);
        System.out.println("Возраст в часах: " + ageInHours);

        long ageInDays = ageInMilliseconds / (1000 * 60 * 60 * 24);
        System.out.println("Возраст в днях: " + ageInDays);

        long ageInYears = ageInDays / 365;
        System.out.println("Возраст в годах: " + ageInYears);

        System.out.println();
    }

    @Override
    public void printResidual(Date date1, Date date2) {

        System.out.println("Дата 1: " + date1);
        System.out.println("Дата 2: " + date2);
        long ageInMilliseconds = abs(date2.getTime() - date1.getTime());

        long ageInDays = ageInMilliseconds / (1000 * 60 * 60 * 24);
        System.out.println("Разница в днях: " + ageInDays);

        System.out.println();

    }


}
