package part1.dateService;

import java.util.Date;

public interface DateService {

    void printAge(Date dateOfBirth);

    void printResidual(Date date1, Date date2);

}