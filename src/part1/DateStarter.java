package part1;

import part1.dateDemo.DateDemoService;
import part1.dateDemo.DateDemoServiceImpl;

public class DateStarter {

    public static void main(String[] args) {

        DateDemoService dateDemoService = new DateDemoServiceImpl();
        dateDemoService.startDemo();

    }
}
