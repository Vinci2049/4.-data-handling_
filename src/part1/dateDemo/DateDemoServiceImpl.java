package part1.dateDemo;

import part1.dateService.DateService;
import part1.dateService.DateServiceImpl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateDemoServiceImpl implements DateDemoService {

    @Override
    public void startDemo() {

        DateService dateService = new DateServiceImpl();

        printAge(dateService);

        printResidual(dateService);

        convertString(dateService);

    }

    // 1.1
    //@Override
    public void printAge(DateService dateService) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            final Date DATEOFBIRTH = dateFormat.parse("12.06.1981 21:00:15");

            dateService.printAge(DATEOFBIRTH);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // 1.2
    //@Override
    public void printResidual(DateService dateService) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

            Date date1 = dateFormat.parse("25.07.1981");
            Date date2 = dateFormat.parse("25.07.1982");
            dateService.printResidual(date1, date2);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void convertString(DateService dateService) {

        // 1.3-1
        String dateString = "Friday, Aug 10, 2016 12:10:56 PM";

        try {
            SimpleDateFormat dateFormatOriginal = new SimpleDateFormat("EEEE, MMM dd, yyyy hh:mm:ss a", Locale.US);
            SimpleDateFormat dateFormatShort = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormatOriginal.parse(dateString);

            System.out.println("Дата в кратком формате: " + dateFormatShort.format(date));

        } catch (Exception e) {
            e.printStackTrace();
        }

        // 1.3-2
        String dateString2 = "2016-08-16T10:15:30+08:00";

        try {
            SimpleDateFormat dateFormatOriginal = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssX", Locale.US);
            //SimpleDateFormat dateFormatShort = new SimpleDateFormat("yyyy-MM-dd");
            //SimpleDateFormat dateFormatLong = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssX", Locale.US);
            Date date = dateFormatOriginal.parse(dateString2);

            //System.out.println("Дата в кратком формате: "+dateFormatShort.format(date));
            //System.out.println("Дата в кратком формате: "+dateFormatLong.format(date));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
